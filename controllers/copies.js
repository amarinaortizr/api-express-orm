const express = require('express');
const { Copie } = require('../db');

//RESTFULL => GET, POST, PATCH, DELETE
//Modelo=(Una estructura de datos que representa una entidad del mundo real)
function list(req, res, next) {
  Copie.findAll({include:['movie', 'bookings']})
          .then(objects => res.json(objects))
          .catch(err => res.send(err));
}

function index(req, res, next){
  const id = req.params.id;
  Copie.findByPk(id)
          .then(object => res.json(object))
          .catch(err => res.send(err));

}

function create(req, res, next){
  const number = req.body.number;
  const format = req.body.format;
  const estaus = req.body.estaus;
  const movieId = req.body.movieId;


  let copie = new Object({
    number:number,
    format:format,
    estaus: estaus,
    movieId: movieId
  });

  Copie.create(copie)
          .then(obj => res.json(obj))
          .catch(err => res.send(err));
}

function replace(req, res, next){
  const id = req.params.id;
  Copie.findByPk(id)
          .then((object) => {
            const number = req.body.number ? req.body.number : "";
            const format = req.body.format ? req.body.format : "";
            const estaus = req.body.estaus ? req.body.estaus : "";
            const movieId = req.body.movieId ? req.body.movieId : "";
            object.update({number:number, format:format, estaus:estaus, movieId:movieId})
                  .then(copie => res.json(copie));
          })
          .catch(err => res.send(err));
}

function edit(req, res, next){
  const id = req.params.id;
  Copie.findByPk(id)
          .then((object) => {
            const number = req.body.number ? req.body.number : object.number;
            const format = req.body.format ? req.body.format : object.format;
            const estaus = req.body.estaus ? req.body.estaus : object.estaus;
            const movieId = req.body.movieId ? req.body.movieId : object.movieId;
            object.update({number:number, format:format, estaus:estaus, movieId:movieId})
                  .then(copie => res.json(copie));
          })
          .catch(err => res.send(err));
}

function destroy(req, res, next){
  const id = req.params.id;
  Copie.destroy({ where:{id:id} })
          .then(obj => res.json(obj))
          .catch(err => rest.send(err));
}

module.exports={
  list, index, create, replace, edit, destroy
}
